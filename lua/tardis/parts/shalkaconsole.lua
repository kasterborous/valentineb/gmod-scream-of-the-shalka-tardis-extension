local PART={}
PART.ID = "shalkaconsole"
PART.Name = "Scream Of The Shalka TARDIS Console"
PART.Model = "models/doctormemes/shalka/rewrite/console.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true
PART.BypassIsomorphic = true
PART.Control = "thirdperson"

TARDIS:AddPart(PART,e)