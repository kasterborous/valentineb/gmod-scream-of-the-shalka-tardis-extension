-- Adds Shalka Rotor

local PART={}
PART.ID = "shalkarotor"
PART.Name = "shalka rotor"
PART.Model = "models/doctormemes/shalka/rewrite/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true

if CLIENT then
	function PART:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	end

	function PART:Think()
		local ext=self.exterior
		if ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex") then
			if self.timerotor.pos==1 then
				self.timerotor.pos=0
			end
				
			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.5 )
			self:SetPoseParameter( "motion", self.timerotor.pos )
		end
	end
end

TARDIS:AddPart(PART)