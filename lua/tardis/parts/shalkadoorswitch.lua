local PART={}
PART.ID = "shalkadoorswitch"
PART.Name = "Shalka Door Switch"
PART.Model = "models/doctormemes/shalka/rewrite/lock.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 4

if SERVER then
	function PART:Use(activator)
		local bigdoors = TARDIS:GetPart(self.interior,"shalkabigdoors")
		self:EmitSound( Sound( "doctormemes/shalka/rewrite/locklever.wav" ))
		bigdoors:Toggle( !bigdoors:GetOn(), activator )
	end
end

TARDIS:AddPart(PART,e)