local PART={}
PART.ID = "shalkathrottle"
PART.Name = "Shalka Throttle"
PART.Model = "models/doctormemes/shalka/rewrite/throttle.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 1.8
PART.Collision = true
PART.Sound = "doctormemes/shalka/rewrite/throttle.wav"
PART.Control = "teleport"

if SERVER then
	function PART:Use(ply)
		if self.exterior:GetData("teleport") == true or self.exterior:GetData("vortex") == true
		or not self.interior:GetSequencesEnabled()
		then
		TARDIS:Control("teleport", ply)
		else
		TARDIS:ErrorMessage(ply, "Control Sequences are enabled. You must use the sequence.")
		end
	end
end

TARDIS:AddPart(PART,e)