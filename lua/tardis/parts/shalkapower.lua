local PART={}
PART.ID = "shalkapower"
PART.Name = "shalka power"
PART.Model = "models/doctormemes/shalka/rewrite/power.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 3.0
PART.Collision = true
PART.Sound = "doctormemes/shalka/rewrite/power.wav"
PART.Control = "power"

if SERVER then

	function PART:Think()

	local power=self.exterior:GetData("power-state")
	local interior=self.interior
	local warning=self.exterior:GetData("health-warning")
		
		if	power == false then
			interior:SetSubMaterial(1 , "models/doctormemes/shalka/yellowglow off")
		else

			if warning == true then
				interior:SetSubMaterial(1 , "models/doctormemes/shalka/yellowglow warn")
			else
				interior:SetSubMaterial(1 , "models/doctormemes/shalka/yellowglow")
			end

		end
	end
end

TARDIS:AddPart(PART,e)