local PART={}
PART.ID = "shalkabigdoors"
PART.Name = "Shalka Big Doors"
PART.Model = "models/doctormemes/shalka/rewrite/bigdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.7

if SERVER then
	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:Use()
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "doctormemes/shalka/rewrite/bigdoors.wav" ))
			self:Collide( true )
		else
			self:EmitSound( Sound( "doctormemes/shalka/rewrite/bigdoors.wav" ))
			self:DontCollide( true )
		end
	end

	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:DontCollide( true )
			self:EmitSound( Sound( "doctormemes/shalka/rewrite/bigdoors.wav" ))
		else
			self:SetOn( false )
			self:Collide( true )
			self:EmitSound( Sound( "doctormemes/shalka/rewrite/bigdoors.wav" ))
		end
	end
end

TARDIS:AddPart(PART,e)