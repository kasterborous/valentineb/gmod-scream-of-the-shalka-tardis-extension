--Scream Of The Shalka TARDIS Interior - Control Sequences (advanced mode)

local Seq = {
    ID = "shalka_sequences",

    ["shalkakeyboard"] = {
        Controls = {
            "shalkaphyslock",
            "shalkalongflight",
            "shalkahandbrake",
	    	"shalkathrottle"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    }
}

TARDIS:AddControlSequence(Seq)