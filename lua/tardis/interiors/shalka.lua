-- Shalka Tardis

local T={}
T.Base="base"
T.Name="Scream Of The Shalka"
T.ID="shalka"
T.Interior={
	Model="models/doctormemes/shalka/rewrite/interior.mdl",
	Portal={
	        pos=Vector(0,619.402,48.48),
	        ang=Angle(0,270,0),
	        width=200,
	        height=220
	},
	Screens={
		{
       	   pos=Vector(-7.45999,-16.2892,80.3739),
        	   ang=Angle(0,0,69.5),
        	   width=240,
        	   height=150,
		   visgui_rows=2
		}
	},
	ScreensEnabled= true,
	Fallback={
		pos=Vector(0,600,6),
		ang=Angle(0,270,0),
	},
	ExitDistance=900,

	Sequences="shalka_sequences",

	Parts={
             door={
                        model="models/doctormemes/shalka/rewrite/intdoor.mdl",posoffset=Vector(20.997,0,-48.4),angoffset=Angle(0,180,0)
               },
	shalkaconsole=true,
	shalkathrottle=true,
	shalkakeyboard=true,
	shalkauseless=true,
	shalkadoorswitch=true,
	shalkaaudiosystem=true,
	shalkaelevator=true,
	shalkasteps=true,
	shalkasteps2=true,
	shalkasteps3=true,
	shalkasteps4=true,
	shalkascreen=true,
	shalkahandbrake=true,
	shalkalongflight=true,
	shalkahelmic=true,
	shalkaphyslock=true,
	shalkaflightmode=true,
	shalkamanual=true,
	shalkapower=true,
	shalkahads=true,
	shalkafastreturn=true,
	shalkavortex=true,
	shalkaphase=true,
	shalkaisomorphic=true,
	shalkarepair=true,
	shalkarotor=true,
	shalkaglass=true,
	shalkabolts=true,
	shalkabolts2=true,
	shalkabigdoors=true,
	
	
		
},

        IdleSound={
		{
			path="doctormemes/shalka/rewrite/interior_idle_loop.wav",
			volume=1	
		}
},
	Light={
	      color=Color(255,247,98),
	      warncolor=Color(255,0,0),
	      pos=Vector(0,0,200),
	      brightness=3
},
	TipSettings={
			style="shalka_tips",
            		view_range_max=73,
            		view_range_min=68,
	},
	PartTips={
		shalkathrottle  =	{pos=Vector(-34.603 , 9.29759 , 60.2325 ) ,text="Demat Switch",  right = true},
        shalkahandbrake	=	{pos=Vector(-28.597 , 19.3475 , 59.6813 ) ,text="Handbrake"},
        shalkalongflight	=	{pos=Vector(-32.182 , 15.2338 , 61.5346 ) ,text="Vortex Flight", down = true},
        shalkakeyboard	=	{pos=Vector(29.6365 , -17.3287 , 60.2094 ) ,text="Coordinates", down = true},
        shalkaphyslock	=	{pos=Vector(26.0234 , -26.1971 , 62.6677 ) ,text="Physical Lock"},
        shalkavortex	=	{pos=Vector(-25.463, -22.84, 61.743 ) ,text="Engine Release", down = true, right = true},
		shalkahelmic	=	{pos=Vector(23.772, -14.172, 63.432 ) ,text="Redecoration"},	
		shalkaaudiosystem	=	{pos=Vector(-16.383, 10.195, 71.578 ) ,text="Music"},	

	},
	CustomTips={
		{pos=Vector(-35.1865 , -9.83447 , 59.0388 ),     text="Cloaking Device", down = true, right = true},
		{pos=Vector(-12.3133 , -35.109 , 59.4104 ),     text="Repair", down = true},
		{pos=Vector(25.7375 , 23.7886 , 61.6909 ),     text="H.A.D.S."},
		{pos=Vector(35.5327 , -9.77089 , 62.7432 ),     text="Flight"},
		{pos=Vector(22.7119 , 30.4466 , 59.7988 ),     text="Fast Return", down = true, right = true},
		{pos=Vector(9.23687 , 36.344 , 62.5129 ),     text="Float"},
		{pos=Vector(31.4938 , 13.8685 , 60.0837 ),     text="Power"},
		{pos=Vector(-3.19584 , 38.6097 , 58.8416 ),     text="Big Doors Switch", right = true},
		{pos=Vector(20.3415 , -5.27303 , 69.1503 ),     text="Visual UI"},
		{pos=Vector(5.50015 , -30.11707 , 61.67394 ),     text="Security"},
	},
}


T.Exterior={
	Model="models/doctormemes/shalka/rewrite/exterior.mdl",
	Mass=5000,
	Portal={
		pos=Vector(21,0,48.4),
		ang=Angle(0,0,0),
		width=32,
		height=92
	},
	Fallback={
		pos=Vector(43,0,7),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=true,
		pos=Vector(0,0,105),
		color=Color(255,255,255)
	},
	Sounds={
		Lock="doctormemes/shalka/rewrite/Lock.wav"
	},
	Parts={
                door={
                        model="models/doctormemes/shalka/rewrite/extdoor.mdl",posoffset=Vector(-20.997,0,-48.4),angoffset=Angle(0,0,0)
                }

	},
	Teleport={
		SequenceSpeed=0.77,
		SequenceSpeedFast=0.935,
		DematSequence={
			175,
			230,
			100,
			150,
			50,
			100,
			0
		},
		MatSequence={
			100,
			50,
			150,
			100,
			200,
			150,
			255
		}
	}
}

TARDIS:AddInterior(T)