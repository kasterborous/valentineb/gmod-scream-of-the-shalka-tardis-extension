local style={
	style_id="shalka_tips",
	style_name="Scream Of The Shalka",
	font = "Trebuchet24",
	colors = {
		normal = {
			text = Color( 255, 255, 255, 255 ),
			background = Color( 5, 130, 5, 250 ),
			frame = Color( 130, 5, 5, 5 ),
		},
		highlighted = {
			text = Color( 255, 255, 255, 255 ),
			background = Color( 5, 5, 220, 250 ),
			frame = Color( 130, 5, 5, 5 ),
		}
	}
}
TARDIS:AddTipStyle(style)